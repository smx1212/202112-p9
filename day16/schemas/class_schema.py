#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/11 10:57
# @Author  : CoderCharm
# @File    : role_schema.py
# @Software: PyCharm
# @Desc    :
"""
权限表
"""
from typing import Optional
from pydantic import BaseModel


class ClassCreate(BaseModel):
    name: str

class Course(BaseModel):
    name: str

class Student(BaseModel):
    name: str
    cid: int

class StuCourse(BaseModel):
    sid: int
    cid: int

class CourseList(BaseModel):
    sid: int
    ids: str
