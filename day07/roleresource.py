#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/11 11:00
# @Author  : CoderCharm
# @File    : role.py
# @Software: PyCharm
# @Desc    :
"""
角色添加资源表crud操作
"""

from typing import Optional
from pydantic import conint
from sqlalchemy import func
from sqlalchemy.orm import Session

from api.common.curd_base import CRUDBase
from api.models.auth import RoleResource, Resource, Role
from ..schemas import role_schema


# 角色添加资源
class CRUDRoleResource(CRUDBase[RoleResource, role_schema.RoleResourceCreate, role_schema.RoleResourceUpdate]):
    def create(self, db: Session, role_id: int, resource_id: str) -> dict:
        # 删除角色对应的所有资源
        query_obj = db.query(RoleResource).filter(
            RoleResource.role_id == role_id).delete()
        db.commit()
        # 解析资源id为列表
        resource_list = resource_id.split(",")
        db_obj = []
        for i in resource_list:
            db_obj = RoleResource(
                role_id=role_id,
                resource_id=i,
            )
            db.add(db_obj)
        db.commit()
        return db_obj

    # 查询角色已经配置的资源
    @staticmethod
    def queryall(db: Session, role_id: int) -> dict:
        """
        查询角色已经配置的资源

        """

        # 查询结果集
        query_obj = db.query(RoleResource).filter(RoleResource.is_delete == 0).filter(
            RoleResource.role_id == role_id).all()

        items = [obj.resource_id for obj in query_obj]

        return {
            "items": items,
        }

    # (clone)
    @staticmethod
    def setResource(db: Session, *, obj_in: role_schema.RoleResourceCreate) -> dict:
        # 删除此角色对应的资源
        db.query(RoleResource).filter(RoleResource.role_id == obj_in.role_id).delete()
        # db.commit()
        # 获取页面传入的资源
        idl = obj_in.resource_id.split(",")

        # 查询此角色是否有继承角色，如果有查询继承角色所对应的资源
        print(obj_in.role_id, "!!!!!!!!!!!!!!!!!!!!!!!!!11")
        roles = db.query(Role).filter(Role.id == obj_in.role_id).first()
        print(roles, "?????????????????????????????")
        if roles.pid > 0:
            pidresource = db.query(RoleResource).filter(RoleResource.role_id == roles.pid).all()
            for i in pidresource:
                resid = str(i.resource_id)
                if resid not in idl:
                    idl.append(resid)

        # [2,3,5]
        for i in idl:
            # 根据i查询互斥表
            sql = "select * from resource_mutex where resourceid1=%d or resourceid2=%d" % (int(i), int(i))
            res = db.execute(sql)
            reslist = []
            for rid in res:
                reslist.append(str(rid.resourceid1))
                reslist.append(str(rid.resourceid2))

            interlist = list(set(idl).intersection(set(reslist)))

            if len(interlist) > 1:
                return False

        for i in idl:
            # 用idl和查询的结果取交集
            # 如果交集大于1
            db_obj = RoleResource(
                role_id=obj_in.role_id,
                resource_id=i
            )
            db.add(db_obj)
        # 将查询到的资源写入数据库
        db.commit()

        return True


crudroleresource = CRUDRoleResource(RoleResource)
