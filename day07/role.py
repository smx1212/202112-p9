#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/11 11:00
# @Author  : CoderCharm
# @File    : role.py
# @Software: PyCharm
# @Desc    :
"""
角色表crud操作
"""

from typing import Optional
from pydantic import conint
from sqlalchemy import func
from sqlalchemy.orm import Session

from api.common.curd_base import CRUDBase
from api.models.auth import Role, Resource,RoleResource
from ..schemas import role_schema


# 角色
class CRUDRole(CRUDBase[Role, role_schema.RoleCreate, role_schema.RoleUpdate]):
    @staticmethod
    def query_role(db: Session, *, role_id: int=0) -> Optional[Role]:
        """
        此role_id是否存在
        :param db:
        :param role_id:
        :return:
        """
        return db.query(Role).filter(Role.role_id == role_id).first()

    def create(self, db: Session, *, obj_in: role_schema.RoleCreate) -> Role:
        db_obj = Role(
            rname=obj_in.rname,
            pid=obj_in.pid,
            roletype=obj_in.roletype
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def query_all(db: Session, *, page: int = 1, page_size: conint(le=50) = 10) -> dict:
        """
        查询数据列表
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size

        # 查询数量
        total = db.query(func.count(Role.id)).filter(Role.is_delete == 0
                                                     ).scalar()
        # 查询结果集
        query_obj = db.query(Role).filter(Role.is_delete == 0).offset(
            temp_page).limit(page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.rname,
                  "pid": obj.pid,"roletype":obj.roletype} for obj in query_obj]
        return {
            "items": items,
            "total": total
        }

    @staticmethod
    def query_inrole(db: Session) -> dict:
        """
        查看所有可继承的角色（roletype=2）

        """

        # 查询结果集
        query_obj = db.query(Role).filter(Role.is_delete == 0).filter(Role.roletype == 2).all()

        items = [{"value": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "label": obj.rname,
                  "pid": obj.pid,"roletype":obj.roletype} for obj in query_obj]
        return {
            "items": items,
        }


curd_role = CRUDRole(Role)


# 资源
class CRUDResource(CRUDBase[Resource, role_schema.ResourceCreate, role_schema.ResourceUpdate]):

    def create(self, db: Session, *, obj_in: role_schema.ResourceCreate) -> Resource:
        db_obj = Resource(
            rname=obj_in.rname,
            pid=obj_in.pid,
            url=obj_in.url,
            type=obj_in.type
        )

        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def query_all(db: Session, *, page: int = 1, page_size: conint(le=50) = 10) -> dict:
        """
        查询数据列表分页
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size

        # 查询数量
        total = db.query(func.count(Resource.id)).filter(Resource.is_delete == 0
                                                         ).scalar()
        # 查询结果集
        query_obj = db.query(Resource).filter(Resource.is_delete == 0).offset(
            temp_page).limit(page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "name": obj.rname,
                  "pid": obj.pid, "url": obj.url, "type": obj.type} for obj in query_obj]
        return {
            "items": items,
            "total": total
        }

    @staticmethod
    def query_two(db: Session) -> dict:
        """
        查询二级资源

        """

        # 查询结果集
        query_obj = db.query(Resource).filter(Resource.is_delete == 0).filter(Resource.pid != 0).all()

        items = [{"key": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "label": obj.rname,
                  "pid": obj.pid, "url": obj.url, "type": obj.type} for obj in query_obj]
        return {
            "items": items,
        }




crudresource_role = CRUDResource(Resource)
