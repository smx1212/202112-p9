#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/7 14:11
# @Author  : CoderCharm
# @File    : views.py
# @Software: PyCharm
# @Desc    :
"""

"""
from datetime import timedelta
from typing import Any, Union
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends
from core import security
from api.common import deps
from api.utils import response_code
from api.common.logger import logger
from core.config import settings
from .schemas import class_schema
from .crud import curd_cclass,curd_course,curd_student,curd_stucourse


router = APIRouter()

@router.get("/getclass", summary="获取所有班级")
async def getclass(db: Session = Depends(deps.get_db)) -> Any:
    res = curd_cclass.get_all(db=db)
    # 转成列表+字典
    data = [{"id":i.id,"name":i.name} for i in res]
    return response_code.resp_200(data = data)

@router.post("/addclass", summary="添加班级")
async def addstudent(class_info:class_schema.ClassCreate,db:Session = Depends(deps.get_db)) -> Any:
    curd_cclass.create(db=db,obj_in=class_info)
    return response_code.resp_200("添加班级成功")

@router.get("/getcourse", summary="获取所有课程")
async def getcourse(db: Session = Depends(deps.get_db)) -> Any:
    res = curd_course.get_all(db=db)
    # 转成列表+字典
    data = [{"id":i.id,"name":i.name} for i in res]
    return response_code.resp_200(data = data)

@router.post("/addcourse", summary="添加课程")
async def addstudent(course_info:class_schema.ClassCreate,db:Session = Depends(deps.get_db)) -> Any:
    curd_cclass.create(db=db,obj_in=course_info)
    return response_code.resp_200("添加课程成功")

@router.get("/getstudent", summary="获取所有学生")
async def getcourse(db: Session = Depends(deps.get_db)) -> Any:
    res = curd_student.get_all(db=db)
    # 转成列表+字典
    data = [{"id":i.id,"name":i.name,"cid":i.cid} for i in res]
    return response_code.resp_200(data = data)

@router.post("/addstudent", summary="添加学生")
async def addstudent(course_info:class_schema.ClassCreate,db:Session = Depends(deps.get_db)) -> Any:
    curd_student.create(db=db,obj_in=course_info)
    return response_code.resp_200("添加学生成功")

@router.get("/getstucourse", summary="获取所有学生和学生该上的课")
async def getstucourse(db: Session = Depends(deps.get_db)) -> Any:
    res = curd_stucourse.get_all(db=db)
    # 转成列表+字典
    data = [{"id":i.id,"sid":i.sid,"cid":i.cid} for i in res]
    return response_code.resp_200(data = data)
# 给学生选课
@router.post("/addstucourse", summary="选课")
async def addstudent(stu_info:class_schema.CourseList,db:Session = Depends(deps.get_db)) -> Any:
    curd_stucourse.create(db=db,obj_in=stu_info)
    return response_code.resp_200("学生添加课程成功")

