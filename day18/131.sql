/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 8.0.23 : Database - oaadmin
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`oaadminss` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `oaadminss`;

/*Table structure for table `admin_role` */

DROP TABLE IF EXISTS `admin_role`;

CREATE TABLE `admin_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `role_id` int NOT NULL COMMENT '角色Id',
  `role_name` varchar(64) DEFAULT NULL COMMENT '角色名字',
  `permission_id` bigint DEFAULT NULL COMMENT '权限ID',
  `re_mark` varchar(128) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`,`role_id`),
  KEY `ix_admin_role_id` (`id`),
  KEY `ix_admin_role_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='管理员角色';

/*Data for the table `admin_role` */

/*Table structure for table `admin_user` */

DROP TABLE IF EXISTS `admin_user`;

CREATE TABLE `admin_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `email` varchar(128) NOT NULL COMMENT '邮箱',
  `phone` varchar(16) DEFAULT NULL COMMENT '手机号',
  `nickname` varchar(128) DEFAULT NULL COMMENT '管理员昵称',
  `avatar` varchar(256) DEFAULT NULL COMMENT '管理员头像',
  `hashed_password` varchar(128) NOT NULL COMMENT '密码',
  `is_active` int DEFAULT '0' COMMENT '邮箱是否激活 0=未激活 1=激活',
  `role_id` int DEFAULT NULL COMMENT '角色表',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_admin_user_email` (`email`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `ix_admin_user_phone` (`phone`),
  KEY `ix_admin_user_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='管理员表';

/*Data for the table `admin_user` */

/*Table structure for table `alembic_version` */

DROP TABLE IF EXISTS `alembic_version`;

CREATE TABLE `alembic_version` (
  `version_num` varchar(32) NOT NULL,
  PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `alembic_version` */

insert  into `alembic_version`(`version_num`) values ('bc35dabbc087');

/*Table structure for table `audit` */

DROP TABLE IF EXISTS `audit`;

CREATE TABLE `audit` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `work_id` int DEFAULT NULL COMMENT '工作流id',
  `task_id` int DEFAULT NULL COMMENT '任务id',
  `audit_user` int DEFAULT NULL COMMENT '审批人',
  `send_user` int DEFAULT NULL COMMENT '最后审批人',
  `message` varchar(100) DEFAULT NULL COMMENT '内容',
  `status` int DEFAULT NULL COMMENT '1同意 2不同意 3回退',
  `audit_time` datetime DEFAULT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`),
  KEY `ix_audit_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='审批记录表';

/*Data for the table `audit` */

/*Table structure for table `classify` */

DROP TABLE IF EXISTS `classify`;

CREATE TABLE `classify` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `cname` varchar(32) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`),
  KEY `ix_classify_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='分类表';

/*Data for the table `classify` */

/*Table structure for table `company` */

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `cname` varchar(25) DEFAULT NULL COMMENT '公司名称',
  `ctype` int DEFAULT NULL COMMENT '公司类型,0：组织，1：部门',
  `coding` varchar(25) DEFAULT NULL COMMENT '公司编码',
  `sort` int DEFAULT NULL COMMENT '公司排序',
  `pid` int DEFAULT NULL COMMENT '公司id',
  PRIMARY KEY (`id`),
  KEY `ix_company_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='公司表';

/*Data for the table `company` */

insert  into `company`(`id`,`create_time`,`update_time`,`is_delete`,`cname`,`ctype`,`coding`,`sort`,`pid`) values (1,'2022-09-18 19:09:14','2022-09-18 19:09:14',0,'积云科技公司',0,'A001',0,0),(2,'2022-09-18 19:28:28','2022-09-18 19:28:28',0,'销售',0,'A001',0,1),(3,'2022-09-18 19:29:09','2022-09-18 19:29:09',0,'工程',1,'A001',10,1),(4,'2022-09-18 21:07:11','2022-09-18 21:07:11',0,'js',1,'A001',10,0),(5,'2022-09-18 21:08:20','2022-09-18 21:08:20',0,'青灯科技公司',0,'A1221',20,0),(6,'2022-09-18 21:13:17','2022-09-18 21:13:17',0,'销售物',1,'A1333',10,5);

/*Table structure for table `dutuies` */

DROP TABLE IF EXISTS `dutuies`;

CREATE TABLE `dutuies` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `dname` varchar(25) DEFAULT NULL COMMENT '职务名称',
  `sort` int DEFAULT NULL COMMENT '排序',
  `level` int DEFAULT NULL COMMENT '级别',
  PRIMARY KEY (`id`),
  KEY `ix_dutuies_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='职务表';

/*Data for the table `dutuies` */

/*Table structure for table `mall_address` */

DROP TABLE IF EXISTS `mall_address`;

CREATE TABLE `mall_address` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `name` varchar(64) DEFAULT NULL COMMENT '用户昵称',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `country_id` int DEFAULT NULL COMMENT '国家Id',
  `province_id` int DEFAULT NULL COMMENT '省id',
  `city_id` int DEFAULT NULL COMMENT '市id',
  `district_id` int DEFAULT NULL COMMENT '区id',
  `address` varchar(128) DEFAULT NULL COMMENT '详细地址',
  `phone` varchar(64) DEFAULT NULL COMMENT '手机号',
  `is_default` smallint DEFAULT '0' COMMENT '是否默认地址',
  PRIMARY KEY (`id`),
  KEY `ix_mall_address_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='地址表';

/*Data for the table `mall_address` */

/*Table structure for table `mall_banner` */

DROP TABLE IF EXISTS `mall_banner`;

CREATE TABLE `mall_banner` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `prc` varchar(256) DEFAULT NULL COMMENT '图片url',
  `link` varchar(256) DEFAULT NULL COMMENT '跳转地址(或者关联商品)',
  `goods_id` varchar(128) DEFAULT NULL COMMENT '关联商品',
  `sort_order` int DEFAULT NULL COMMENT '排序',
  `enabled` smallint DEFAULT '1' COMMENT '是否开启 0=为开启 1=开启',
  `banner_position` varchar(64) DEFAULT NULL COMMENT '轮播图位置 默认首页 首页home 个人中心profile ',
  PRIMARY KEY (`id`),
  KEY `ix_mall_banner_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `mall_banner` */

/*Table structure for table `mall_cart` */

DROP TABLE IF EXISTS `mall_cart`;

CREATE TABLE `mall_cart` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `goods_id` varchar(20) DEFAULT NULL COMMENT '商品id',
  `goods_num` int DEFAULT NULL COMMENT '数量',
  `retail_price` decimal(10,2) DEFAULT NULL COMMENT '零售价,单价',
  PRIMARY KEY (`id`),
  KEY `ix_mall_cart_goods_id` (`goods_id`),
  KEY `ix_mall_cart_id` (`id`),
  KEY `ix_mall_cart_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='购物车';

/*Data for the table `mall_cart` */

/*Table structure for table `mall_category` */

DROP TABLE IF EXISTS `mall_category`;

CREATE TABLE `mall_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `name` varchar(64) DEFAULT NULL COMMENT '分类名称',
  `front_desc` varchar(256) DEFAULT NULL COMMENT '分类描述',
  `parent_id` int DEFAULT NULL COMMENT '父id',
  `sort_order` smallint DEFAULT NULL COMMENT '排序',
  `icon_url` varchar(256) DEFAULT NULL COMMENT '分类显示icon',
  `enabled` smallint DEFAULT '1' COMMENT '是否开启 0=为开启 1=开启',
  PRIMARY KEY (`id`),
  KEY `ix_mall_category_id` (`id`),
  KEY `ix_mall_category_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `mall_category` */

/*Table structure for table `mall_except_area` */

DROP TABLE IF EXISTS `mall_except_area`;

CREATE TABLE `mall_except_area` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `content` varchar(256) DEFAULT NULL COMMENT '地区名称',
  `area` varchar(256) DEFAULT NULL COMMENT '地区地区id ,隔开如 6,30,31,32',
  PRIMARY KEY (`id`),
  KEY `ix_mall_except_area_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Data for the table `mall_except_area` */

/*Table structure for table `mall_freight_template` */

DROP TABLE IF EXISTS `mall_freight_template`;

CREATE TABLE `mall_freight_template` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `name` varchar(128) DEFAULT NULL COMMENT '运费模版名称',
  `package_price` decimal(10,2) DEFAULT NULL COMMENT '包装费用',
  `freight_type` smallint DEFAULT NULL COMMENT '0=按件 1=按重量',
  PRIMARY KEY (`id`),
  KEY `ix_mall_freight_template_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='运费模版';

/*Data for the table `mall_freight_template` */

/*Table structure for table `mall_goods` */

DROP TABLE IF EXISTS `mall_goods`;

CREATE TABLE `mall_goods` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `goods_id` varchar(128) DEFAULT NULL COMMENT '商品id',
  `category_id` int DEFAULT NULL COMMENT '分类id',
  `is_on_sale` int DEFAULT NULL COMMENT '是否售卖 0=否 1=是',
  `goods_name` varchar(64) DEFAULT NULL COMMENT '商品名称',
  `goods_number` int DEFAULT NULL COMMENT '商品数量',
  `pic_banner` varchar(256) DEFAULT NULL COMMENT '商品展示banner没有取商品轮播图',
  `list_pic_url` varchar(256) DEFAULT NULL COMMENT '商品列表轮播图',
  `specification_id` int DEFAULT NULL COMMENT '商品规格id',
  `keywords` varchar(256) DEFAULT NULL COMMENT '商品关键字',
  `sell_volume` int DEFAULT NULL COMMENT '销售量',
  `retail_price` decimal(10,2) DEFAULT NULL COMMENT '零售价,单价',
  `min_retail_price` decimal(10,2) DEFAULT NULL COMMENT '最低零售价',
  `cost_price` decimal(10,2) DEFAULT NULL COMMENT '成本价',
  `min_cost_price` decimal(10,2) DEFAULT NULL COMMENT '最低成本价',
  `goods_brief` varchar(256) DEFAULT NULL COMMENT '商品简介',
  `goods_desc` text COMMENT '商品描述',
  `sort_order` smallint DEFAULT NULL COMMENT '排序',
  `is_index` smallint DEFAULT NULL,
  `is_new` smallint DEFAULT NULL COMMENT '是否新品推荐',
  `goods_unit` varchar(45) DEFAULT NULL COMMENT '商品单位',
  `freight_template_id` smallint DEFAULT NULL COMMENT '配运模版id',
  `freight_type` varchar(256) DEFAULT NULL COMMENT '配运类型',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_mall_goods_goods_id` (`goods_id`),
  KEY `ix_mall_goods_category_id` (`category_id`),
  KEY `ix_mall_goods_goods_number` (`goods_number`),
  KEY `ix_mall_goods_id` (`id`),
  KEY `ix_mall_goods_sort_order` (`sort_order`),
  KEY `ix_mall_goods_specification_id` (`specification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品详情';

/*Data for the table `mall_goods` */

/*Table structure for table `mall_goods_gallery` */

DROP TABLE IF EXISTS `mall_goods_gallery`;

CREATE TABLE `mall_goods_gallery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `goods_id` smallint DEFAULT NULL COMMENT '商品id',
  `image_url` varchar(256) DEFAULT NULL,
  `image_desc` varchar(64) DEFAULT NULL COMMENT '描述',
  `sort_order` smallint DEFAULT NULL COMMENT '排序',
  `enabled` smallint DEFAULT '1' COMMENT '是否开启 0=为开启 1=开启',
  PRIMARY KEY (`id`),
  KEY `ix_mall_goods_gallery_goods_id` (`goods_id`),
  KEY `ix_mall_goods_gallery_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品列表';

/*Data for the table `mall_goods_gallery` */

/*Table structure for table `mall_goods_keywords` */

DROP TABLE IF EXISTS `mall_goods_keywords`;

CREATE TABLE `mall_goods_keywords` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `keyword` varchar(128) DEFAULT NULL COMMENT '关键词',
  `is_hot` smallint DEFAULT NULL COMMENT '是否热点',
  `is_default` smallint DEFAULT NULL COMMENT '是否热点',
  `is_show` smallint DEFAULT NULL COMMENT '是否展示',
  `sort_order` int DEFAULT NULL COMMENT '排序',
  `scheme_url` varchar(128) DEFAULT NULL COMMENT '关键词跳转链接',
  PRIMARY KEY (`id`),
  KEY `ix_mall_goods_keywords_id` (`id`),
  KEY `ix_mall_goods_keywords_is_hot` (`is_hot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品关键词';

/*Data for the table `mall_goods_keywords` */

/*Table structure for table `mall_goods_specification` */

DROP TABLE IF EXISTS `mall_goods_specification`;

CREATE TABLE `mall_goods_specification` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `good_id` varchar(128) DEFAULT NULL COMMENT '对应商品id',
  `name` varchar(64) DEFAULT NULL COMMENT '商品规格名称 如重量 长度 颜色',
  `unit` varchar(16) DEFAULT NULL COMMENT '规格单位 如斤 克',
  `memo` varchar(64) DEFAULT NULL COMMENT '补充说明 如每袋5个装',
  `stock` int DEFAULT NULL COMMENT '库存',
  PRIMARY KEY (`id`),
  KEY `ix_mall_goods_specification_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='商品规格';

/*Data for the table `mall_goods_specification` */

/*Table structure for table `mall_order` */

DROP TABLE IF EXISTS `mall_order`;

CREATE TABLE `mall_order` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `order_id` varchar(20) DEFAULT NULL COMMENT '订单id',
  `user_id` int DEFAULT NULL COMMENT '用户Id',
  `phone` varchar(64) DEFAULT NULL COMMENT '手机号',
  `order_status` smallint DEFAULT NULL COMMENT '订单状态 101：未付款、102：已取消、103已取消(系统)、201：已付款、202：订单取消，退款中、203：已退款、301：已发货、302：已收货、303：已收货(系统)、401：已完成、801：拼团中,未付款、802：拼团中,已付款',
  `offline_pay` int DEFAULT NULL COMMENT '线下支付订单标志',
  `shipping_status` int DEFAULT NULL COMMENT '发货状态',
  `consignee` varchar(64) DEFAULT NULL COMMENT '支付状态',
  `country_id` int DEFAULT NULL COMMENT '国家',
  `province_id` int DEFAULT NULL COMMENT '省',
  `city_id` int DEFAULT NULL COMMENT '市',
  `district_id` int DEFAULT NULL COMMENT '区',
  `address` varchar(128) DEFAULT NULL COMMENT '详细地址',
  `postscript` varchar(128) DEFAULT NULL COMMENT '用户留言备注',
  `admin_memo` varchar(128) DEFAULT NULL COMMENT '管理员留言备注',
  `print_status` int DEFAULT NULL COMMENT '打印状态',
  `print_info` varchar(128) DEFAULT NULL COMMENT '打印信息',
  `shipping_fee` decimal(10,2) DEFAULT NULL COMMENT '免邮的商品的邮费，这个在退款时不能退给客户',
  `pay_name` varchar(128) DEFAULT NULL COMMENT '支付名称',
  `pay_id` varchar(255) DEFAULT NULL COMMENT '支付ID',
  `pay_status` int DEFAULT NULL COMMENT '支付状态',
  `change_price` decimal(10,2) DEFAULT NULL COMMENT '0没改价，不等于0改过价格，这里记录原始的价格',
  `actual_price` decimal(10,2) DEFAULT NULL COMMENT '实际需要支付的金额',
  `order_price` decimal(10,2) DEFAULT NULL COMMENT '订单总价',
  `goods_price` decimal(10,2) DEFAULT NULL COMMENT '商品总价',
  `add_time` datetime DEFAULT NULL COMMENT '添加时间',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `shipping_time` datetime DEFAULT NULL COMMENT '发货时间',
  `confirm_time` datetime DEFAULT NULL COMMENT '确认时间',
  `dealdone_time` datetime DEFAULT NULL COMMENT '成交时间，用户评论或自动好评时间',
  `freight_price` int DEFAULT NULL COMMENT '配送费用',
  `express_value` decimal(10,2) DEFAULT NULL COMMENT '顺丰保价金额',
  `remark` varchar(256) DEFAULT NULL,
  `order_type` smallint DEFAULT NULL COMMENT '订单类型：0普通，1秒杀，2团购，3返现订单,7充值，8会员',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_mall_order_order_id` (`order_id`),
  KEY `ix_mall_order_id` (`id`),
  KEY `ix_mall_order_order_status` (`order_status`),
  KEY `ix_mall_order_pay_status` (`pay_status`),
  KEY `ix_mall_order_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='订单表';

/*Data for the table `mall_order` */

/*Table structure for table `mall_order_express` */

DROP TABLE IF EXISTS `mall_order_express`;

CREATE TABLE `mall_order_express` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `order_id` varchar(20) DEFAULT NULL COMMENT '订单id',
  `shipper_id` varchar(20) DEFAULT NULL COMMENT '物流公司Id',
  `shipper_name` varchar(20) DEFAULT NULL COMMENT '物流公司名称',
  `shipper_code` varchar(20) DEFAULT NULL COMMENT '物流公司代码',
  `logistic_code` varchar(20) DEFAULT NULL COMMENT '快递单号',
  `traces` varchar(20) DEFAULT NULL COMMENT '物流跟踪信息',
  `is_finish` smallint DEFAULT NULL COMMENT '是否完成 0否 1是',
  `request_count` int DEFAULT NULL COMMENT '总查询次数',
  `request_time` datetime DEFAULT NULL COMMENT '最近一次查询时间',
  `express_type` int DEFAULT NULL COMMENT '快递类型',
  `region_code` int DEFAULT NULL COMMENT '快递的地区编码，如杭州571',
  PRIMARY KEY (`id`),
  KEY `ix_mall_order_express_id` (`id`),
  KEY `ix_mall_order_express_order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='订单物流表';

/*Data for the table `mall_order_express` */

/*Table structure for table `mall_order_goods` */

DROP TABLE IF EXISTS `mall_order_goods`;

CREATE TABLE `mall_order_goods` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `order_id` varchar(20) DEFAULT NULL COMMENT '订单id',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `goods_id` varchar(20) DEFAULT NULL COMMENT '商品id',
  `goods_name` varchar(20) DEFAULT NULL COMMENT '商品名称',
  `goods_aka` varchar(20) DEFAULT NULL COMMENT '商品别称',
  `number` smallint DEFAULT NULL COMMENT '数量',
  `retail_price` decimal(10,2) DEFAULT NULL COMMENT '零售价',
  `goods_specifition_name_value` varchar(256) DEFAULT NULL COMMENT '规格',
  `goods_specifition_ids` varchar(256) DEFAULT NULL COMMENT '商品规格id',
  `list_pic_url` varchar(256) DEFAULT NULL COMMENT '商品图片',
  PRIMARY KEY (`id`),
  KEY `ix_mall_order_goods_goods_id` (`goods_id`),
  KEY `ix_mall_order_goods_id` (`id`),
  KEY `ix_mall_order_goods_order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='订单商品表';

/*Data for the table `mall_order_goods` */

/*Table structure for table `mall_region` */

DROP TABLE IF EXISTS `mall_region`;

CREATE TABLE `mall_region` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `parent_id` smallint DEFAULT NULL COMMENT '父id',
  `region_name` varchar(128) DEFAULT NULL COMMENT '地区名称',
  `region_type` smallint DEFAULT NULL COMMENT '地区类型',
  `agency_id` smallint DEFAULT NULL,
  `area` varchar(64) DEFAULT NULL COMMENT '方位，根据这个定运费',
  `area_code` varchar(8) DEFAULT NULL COMMENT '方位代码',
  `far_area` smallint DEFAULT NULL COMMENT '偏远地区',
  PRIMARY KEY (`id`),
  KEY `ix_mall_region_agency_id` (`agency_id`),
  KEY `ix_mall_region_id` (`id`),
  KEY `ix_mall_region_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='地区';

/*Data for the table `mall_region` */

/*Table structure for table `mall_search_history` */

DROP TABLE IF EXISTS `mall_search_history`;

CREATE TABLE `mall_search_history` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `keyword` varchar(64) DEFAULT NULL COMMENT '搜索关键词',
  `search_origin` smallint DEFAULT '1' COMMENT '搜索来源 1=小程序 2=APP 3=PC',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`),
  KEY `ix_mall_search_history_id` (`id`),
  KEY `ix_mall_search_history_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='搜索记录';

/*Data for the table `mall_search_history` */

/*Table structure for table `mall_shipper` */

DROP TABLE IF EXISTS `mall_shipper`;

CREATE TABLE `mall_shipper` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `shipper_name` varchar(128) DEFAULT NULL COMMENT '物流公司名称',
  `shipper_code` varchar(128) DEFAULT NULL COMMENT '物流公司代码',
  `sort_order` int DEFAULT NULL COMMENT '排序',
  `month_code` varchar(16) DEFAULT NULL COMMENT '月份',
  `customer_name` varchar(16) DEFAULT NULL COMMENT '用户名',
  `enabled` smallint DEFAULT '1' COMMENT '是否开启 0=为开启 1=开启',
  PRIMARY KEY (`id`),
  KEY `ix_mall_shipper_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='快递信息';

/*Data for the table `mall_shipper` */

/*Table structure for table `mall_site_notice` */

DROP TABLE IF EXISTS `mall_site_notice`;

CREATE TABLE `mall_site_notice` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `enabled` smallint DEFAULT '1' COMMENT '是否开启 0=为开启 1=开启',
  `content` varchar(256) DEFAULT NULL COMMENT '全局消息通知',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`),
  KEY `ix_mall_site_notice_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='站点消息';

/*Data for the table `mall_site_notice` */

/*Table structure for table `mall_user` */

DROP TABLE IF EXISTS `mall_user`;

CREATE TABLE `mall_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `user_id` varchar(32) DEFAULT NULL COMMENT '用户id',
  `nickname` varchar(128) DEFAULT NULL COMMENT '用户昵称(显示用可更改)',
  `username` varchar(128) DEFAULT NULL COMMENT '用户名(不可更改)',
  `avatar` varchar(256) DEFAULT NULL COMMENT '用户头像',
  `hashed_password` varchar(128) NOT NULL COMMENT '密码',
  `phone` varchar(16) DEFAULT NULL COMMENT '手机号',
  `gender` smallint DEFAULT '0' COMMENT '性别 0=未知 1=男 2=女',
  `register_time` datetime DEFAULT NULL COMMENT '注册事件',
  `last_login_time` datetime DEFAULT NULL COMMENT '上次登录时间',
  `last_login_ip` varchar(64) DEFAULT NULL COMMENT '上次登录IP',
  `register_ip` varchar(64) DEFAULT NULL COMMENT '注册IP',
  `weixin_openid` varchar(64) DEFAULT NULL COMMENT '微信openId',
  `country` varchar(64) DEFAULT NULL COMMENT '国家',
  `province` varchar(64) DEFAULT NULL COMMENT '省',
  `city` varchar(64) DEFAULT NULL COMMENT '市',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_mall_user_phone` (`phone`),
  UNIQUE KEY `ix_mall_user_user_id` (`user_id`),
  KEY `ix_mall_user_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';

/*Data for the table `mall_user` */

/*Table structure for table `mutual` */

DROP TABLE IF EXISTS `mutual`;

CREATE TABLE `mutual` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `resource_id1` int DEFAULT NULL COMMENT '一级资源id',
  `resource_id2` int DEFAULT NULL COMMENT '二级资源id',
  PRIMARY KEY (`id`),
  KEY `ix_mutual_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色配置资源的互斥表';

/*Data for the table `mutual` */

insert  into `mutual`(`id`,`create_time`,`update_time`,`is_delete`,`resource_id1`,`resource_id2`) values (1,'2022-09-19 15:55:06','2022-09-19 15:55:06',0,1,3),(2,'2022-09-19 15:55:12','2022-09-19 15:55:12',0,4,1);

/*Table structure for table `resource` */

DROP TABLE IF EXISTS `resource`;

CREATE TABLE `resource` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `rname` varchar(25) DEFAULT NULL COMMENT '名称',
  `pid` int DEFAULT NULL COMMENT '0菜单  1资源',
  `url` varchar(300) DEFAULT NULL COMMENT '路由',
  `type` int DEFAULT NULL COMMENT '1接口权限 2页面',
  `promition` int DEFAULT NULL COMMENT '二进制运算',
  PRIMARY KEY (`id`),
  KEY `ix_resource_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='资源表';

/*Data for the table `resource` */

insert  into `resource`(`id`,`create_time`,`update_time`,`is_delete`,`rname`,`pid`,`url`,`type`,`promition`) values (1,'2022-09-18 19:24:33','2022-09-18 19:24:33',0,'添加角色',0,'admin/rbacm/add/role',1,NULL),(3,'2022-09-19 08:41:40','2022-09-19 08:41:40',0,'资源3',1,'admin/rbacm/add/resource',1,2),(4,'2022-09-19 08:41:47','2022-09-19 08:41:47',0,'资源4',1,'admin/rbacm/add/role',1,4),(5,'2022-09-19 08:41:54','2022-09-19 08:41:54',0,'资源5',1,'admin/rbacm/add/role/resource',1,8),(6,'2022-09-19 08:42:00','2022-09-19 08:42:00',0,'资源6',1,'admin/rbacm/add/resource',1,16),(7,'2022-09-19 08:42:21','2022-09-19 08:42:21',0,'资源7',0,'admin/rbacm/add/role/resource',2,NULL),(8,'2022-09-20 14:36:12','2022-09-20 14:36:12',0,'2222',1,'admin/rbacm/add/resource',1,32),(9,'2022-09-20 22:06:23','2022-09-20 22:06:23',0,'123',1,'admin/rbacm/add/role',1,64),(10,'2022-09-20 22:38:26','2022-09-20 22:38:26',0,'组织用户',0,'admin/rbacm/add/role',2,0),(11,'2022-09-20 22:38:57','2022-09-20 22:38:57',0,'菜单权限',0,'admin/rbacm/add/role',2,0),(12,'2022-09-20 22:39:19','2022-09-20 22:39:19',0,'应用管理',0,'admin/rbacm/add/resource',2,0),(13,'2022-09-20 22:40:24','2022-09-20 22:40:24',0,'组织管理',10,'/company',1,128),(14,'2022-09-20 22:40:24','2022-09-20 22:40:24',0,'用户管理',10,'/Resource',1,256),(16,'2022-09-20 22:41:51','2022-09-20 22:41:51',0,'职务管理',10,'/Resource',1,512),(17,'2022-09-20 22:42:05','2022-09-20 22:42:05',0,'岗位管理',10,'/Resource',1,1024),(18,'2022-09-20 22:42:26','2022-09-20 22:42:26',0,'角色管理',11,'/role',1,2048),(19,'2022-09-20 22:42:40','2022-09-20 22:42:40',0,'资源管理',11,'/Resource',1,4096),(20,'2022-09-20 22:42:58','2022-09-20 22:42:58',0,'app',12,'/role',1,8192),(21,'2022-09-20 22:43:06','2022-09-20 22:43:06',0,'api',12,'/Resource',1,16384),(22,'2022-09-23 14:57:24','2022-09-23 14:57:24',0,'工作流管理',11,'/work',1,32768);

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `rname` varchar(25) DEFAULT NULL COMMENT '角色名称',
  `pid` int DEFAULT NULL COMMENT '分类（自关联）',
  `roletype` int DEFAULT NULL COMMENT '类型（1用户角色， 2继承角色）',
  `promition` int DEFAULT NULL COMMENT '二进制运算',
  PRIMARY KEY (`id`),
  KEY `ix_role_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色表';

/*Data for the table `role` */

insert  into `role`(`id`,`create_time`,`update_time`,`is_delete`,`rname`,`pid`,`roletype`,`promition`) values (1,'2022-09-18 19:23:47','2022-09-18 19:23:47',0,'工程师',0,2,26),(2,'2022-09-18 19:24:03','2022-09-18 19:24:03',0,'python工程师',1,1,39322),(4,'2022-09-19 08:36:41','2022-09-19 08:36:41',0,'java工程师',1,1,2074),(5,'2022-09-19 10:43:56','2022-09-19 10:43:56',0,'销售',0,2,6),(6,'2022-09-19 10:44:57','2022-09-19 10:44:57',0,'php工程师',1,1,NULL),(7,'2022-09-19 10:47:48','2022-09-19 10:47:48',0,'美化',0,2,10),(8,'2022-09-19 10:55:02','2022-09-19 10:55:02',0,'111',0,2,24),(9,'2022-09-19 10:56:20','2022-09-19 10:56:20',0,'2222',8,1,24),(10,'2022-09-19 10:59:17','2022-09-19 10:59:17',0,'2222',0,2,16),(11,'2022-09-19 10:59:29','2022-09-19 10:59:29',0,'22',10,1,20),(12,'2022-09-19 11:11:27','2022-09-19 11:11:27',0,'333',0,2,48),(13,'2022-09-19 15:22:58','2022-09-19 15:22:58',0,'3',12,1,0),(14,'2022-09-20 19:15:06','2022-09-20 19:15:06',0,'555',1,1,NULL),(15,'2022-09-20 21:22:59','2022-09-20 21:22:59',0,'python销售',5,1,NULL),(16,'2022-09-23 17:19:13','2022-09-23 17:19:13',0,'工作流',0,2,6),(17,'2022-09-23 17:19:26','2022-09-23 17:19:26',0,'总经理',16,1,NULL),(18,'2022-09-23 17:19:37','2022-09-23 17:19:37',0,'副总经理',16,1,NULL),(19,'2022-09-23 17:19:45','2022-09-23 17:19:45',0,'总监',16,1,NULL),(20,'2022-09-23 17:19:53','2022-09-23 17:19:53',0,'主管',16,1,NULL);

/*Table structure for table `role_resource` */

DROP TABLE IF EXISTS `role_resource`;

CREATE TABLE `role_resource` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `role_id` int DEFAULT NULL COMMENT '角色id',
  `resource_id` int DEFAULT NULL COMMENT '资源id',
  PRIMARY KEY (`id`),
  KEY `ix_role_resource_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色资源表';

/*Data for the table `role_resource` */

insert  into `role_resource`(`id`,`create_time`,`update_time`,`is_delete`,`role_id`,`resource_id`) values (37,'2022-09-20 22:13:40','2022-09-20 22:13:40',0,11,4),(64,'2022-09-21 11:45:31','2022-09-21 11:45:31',0,4,18),(97,'2022-09-23 15:04:35','2022-09-23 15:04:35',0,2,18),(98,'2022-09-23 15:04:35','2022-09-23 15:04:35',0,2,13),(99,'2022-09-23 15:04:35','2022-09-23 15:04:35',0,2,14),(100,'2022-09-23 15:04:35','2022-09-23 15:04:35',0,2,19),(101,'2022-09-23 15:04:35','2022-09-23 15:04:35',0,2,22);

/*Table structure for table `station` */

DROP TABLE IF EXISTS `station`;

CREATE TABLE `station` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `sname` varchar(25) DEFAULT NULL COMMENT '岗位名称',
  `sort` int DEFAULT NULL COMMENT '排序',
  `desc` varchar(60) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`id`),
  KEY `ix_station_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='岗位表';

/*Data for the table `station` */

/*Table structure for table `task` */

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `work_id` int DEFAULT NULL COMMENT '工作流id',
  `user_id` int DEFAULT NULL COMMENT '用户id',
  `message` varchar(100) DEFAULT NULL COMMENT '内容',
  `audit_list` varchar(200) DEFAULT NULL COMMENT '审批人列表',
  `next_audit` varchar(200) DEFAULT NULL COMMENT '下一审批人',
  `status` int DEFAULT NULL COMMENT '1开始  2审批人中  3已完成',
  PRIMARY KEY (`id`),
  KEY `ix_task_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='任务表';

/*Data for the table `task` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `uname` varchar(25) DEFAULT NULL COMMENT '姓名',
  `account` varchar(25) DEFAULT NULL COMMENT '登录账号',
  `number` varchar(30) DEFAULT NULL COMMENT '工号',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机号',
  `company_id` int DEFAULT NULL COMMENT '所属公司',
  `station_id` int DEFAULT NULL COMMENT '岗位id',
  `dutuies_id` int DEFAULT NULL COMMENT '职务id',
  `role_id` int DEFAULT NULL COMMENT '角色id',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  PRIMARY KEY (`id`),
  KEY `ix_user_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户表';

/*Data for the table `user` */

insert  into `user`(`id`,`create_time`,`update_time`,`is_delete`,`uname`,`account`,`number`,`mobile`,`company_id`,`station_id`,`dutuies_id`,`role_id`,`password`) values (1,'2022-09-22 20:00:50','2022-09-22 20:00:50',0,'张三','111111','23532345','13111111111',1,1,1,1,'111');

/*Table structure for table `work` */

DROP TABLE IF EXISTS `work`;

CREATE TABLE `work` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `cname` varchar(32) DEFAULT NULL COMMENT '名称',
  `reason` varchar(32) DEFAULT NULL COMMENT '理由',
  `status` int DEFAULT NULL COMMENT '1没审核  2通过  3没通过',
  `parmars` text COMMENT '属性',
  PRIMARY KEY (`id`),
  KEY `ix_work_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='工作流表';

/*Data for the table `work` */

insert  into `work`(`id`,`create_time`,`update_time`,`is_delete`,`cname`,`reason`,`status`,`parmars`) values (2,'2022-09-23 11:37:47','2022-09-23 16:06:37',0,'请假工作流','1',1,'[{\"name\":\"发烧\",\"type\":\"select\",\"value\":{\"name\":\"zhangsan\",\"type\":\"38.3\"}}]'),(3,'2022-09-23 11:38:07','2022-09-23 16:10:19',0,'报销工作流','1',1,'[{\"name\":\"发烧\",\"type\":\"select\",\"value\":{\"name\":\"zhangsan\",\"type\":\"38.3\"}}]');

/*Table structure for table `work_audit` */

DROP TABLE IF EXISTS `work_audit`;

CREATE TABLE `work_audit` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `min` int DEFAULT NULL COMMENT '最小天数',
  `max` int DEFAULT NULL COMMENT '最大天数',
  `work_id` int DEFAULT NULL COMMENT '工作流id',
  `id_list` varchar(200) DEFAULT NULL COMMENT '审核人id',
  PRIMARY KEY (`id`),
  KEY `ix_work_audit_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='工作流审核表';

/*Data for the table `work_audit` */

insert  into `work_audit`(`id`,`create_time`,`update_time`,`is_delete`,`min`,`max`,`work_id`,`id_list`) values (1,'2022-09-24 12:22:10','2022-09-24 12:22:10',0,1,3,0,'[18, 19]'),(2,'2022-09-24 12:24:22','2022-09-24 12:24:22',0,1,3,0,'[20, 17]'),(3,'2022-09-24 12:25:04','2022-09-24 12:25:04',0,1,3,3,'[20, 17]');

/*Table structure for table `work_right` */

DROP TABLE IF EXISTS `work_right`;

CREATE TABLE `work_right` (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `is_delete` int DEFAULT '0' COMMENT '逻辑删除:0=未删除,1=删除',
  `role_id` int DEFAULT NULL COMMENT '角色id',
  `work_id` int DEFAULT NULL COMMENT '工作流id',
  PRIMARY KEY (`id`),
  KEY `ix_work_right_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='工作流权限表';

/*Data for the table `work_right` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
