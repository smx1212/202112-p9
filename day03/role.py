#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/7/11 11:00
# @Author  : CoderCharm
# @File    : role.py
# @Software: PyCharm
# @Desc    :
"""
角色表crud操作
"""

from typing import Optional

from pydantic import conint
from sqlalchemy import func
from sqlalchemy.orm import Session

from api.common.curd_base import CRUDBase
from api.models.auth import Role, Resource

from ..schemas import role_schema, resource_schema


class CRUDRole(CRUDBase[Role, role_schema.RoleCreate, role_schema.RoleUpdate]):

    @staticmethod
    def query_role(db: Session, *, role_id: int) -> Optional[Role]:
        """
        此role_id是否存在
        :param db:
        :param role_id:
        :return:
        """
        return db.query(Role).filter(Role.role_id == role_id).first()

    def create(self, db: Session, *, obj_in: role_schema.RoleCreate) -> Role:
        db_obj = Role(
            rname=obj_in.rname,
            pip=obj_in.pip
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def query_all(db: Session, *, page: int = 1, page_size: conint(le=50) = 10) -> dict:
        """
        查询数据列表
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size
        # 查询数量
        total = db.query(func.count(Role.id)).filter(Role.is_delete == 0).scalar()
        # 查询结果集
        query_obj = db.query(Role).filter(Role.is_delete == 0).offset(
            temp_page).limit(page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "rname": obj.rname, "pip": obj.pip} for obj in query_obj]
        return {
            "items": items,
            "total": total
        }


curd_role = CRUDRole(Role)


class CRUDResource(CRUDBase[Resource, resource_schema.ResourceCreate, resource_schema.ResourceUpdate]):

    @staticmethod
    def query_role(db: Session, *, resource_id: int) -> Optional[Role]:
        """
        此role_id是否存在
        :param db:
        :param role_id:
        :return:
        """
        return db.query(Resource).filter(Resource.role_id == resource_id).first()

    def create(self, db: Session, *, obj_in: resource_schema.ResourceCreate) -> Resource:
        db_obj = Resource(
            rname=obj_in.rname,
            pid=obj_in.pid,
            url=obj_in.url,
            type=obj_in.type
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    @staticmethod
    def query_all(db: Session, *, page: int = 1, page_size: conint(le=50) = 10) -> dict:
        """
        查询数据列表
        :param db:
        :param page:
        :param page_size:
        :return:
        """
        temp_page = (page - 1) * page_size
        # 查询数量
        total = db.query(func.count(Resource.id)).filter(Resource.is_delete == 0).scalar()
        # 查询结果集
        query_obj = db.query(Resource).filter(Resource.is_delete == 0).offset(
            temp_page).limit(page_size).all()

        items = [{"id": obj.id, 'create_time': obj.create_time.strftime('%Y-%m-%d %H:%M:%S'), "rname": obj.rname, "pid":
            obj.pid, 'url': obj.url, 'type': obj.type} for obj in query_obj]
        return {
            "items": items,
            "total": total
        }


curd_resource = CRUDResource(Resource)
