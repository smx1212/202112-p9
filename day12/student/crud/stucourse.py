from typing import Optional
from sqlalchemy.orm import Session
from api.common.curd_base import CRUDBase
# from api.models.student import *
from api.models.student import StuCourse
from ..schemas import class_schema

class CRUDStucourse(CRUDBase[StuCourse, class_schema.StuCourse, None]):
    def create(self, db: Session, *, obj_in: class_schema.StuCourse) -> StuCourse:
        db_obj = StuCourse(
            cid = obj_in.sid,
            sid = obj_in.cid,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

curd_stucourse = CRUDStucourse(StuCourse)
