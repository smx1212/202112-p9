from typing import Optional
from sqlalchemy.orm import Session
from api.common.curd_base import CRUDBase
# from api.models.student import *
from api.models.student import Course
from ..schemas import class_schema



class CRUDCclass(CRUDBase[Course, None, None]):
    def create(self, db: Session, *, obj_in: class_schema.ClassCreate) -> Course:
        db_obj = Course(
            name=obj_in.name,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    # def setStuCourse(self,db:Session, * ,obj_in: class_schema.CourseList) ->None:
    #     sid = obj_in.sid
    #     ids = obj_in.ids
    #     # 根据学生删除关系表中对应的数据
    #     db.query(StuCourse).filter().delete()
    #     idlist = ids.split(",")
        # 添加

curd_course = CRUDCclass(Course)
