from typing import Optional
from sqlalchemy.orm import Session
from api.common.curd_base import CRUDBase
# from api.models.student import *
from api.models.student import Sclass
from ..schemas import class_schema



class CRUDCclass(CRUDBase[Sclass, class_schema.ClassCreate, None]):
    def create(self, db: Session, *, obj_in: class_schema.ClassCreate) -> Sclass:
        db_obj = Sclass(
            name=obj_in.name,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

curd_cclass = CRUDCclass(Sclass)
