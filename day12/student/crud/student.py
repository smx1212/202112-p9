from typing import Optional
from sqlalchemy.orm import Session
from api.common.curd_base import CRUDBase
# from api.models.student import *
from api.models.student import Student
from ..schemas import class_schema

class CRUDStudent(CRUDBase[Student, class_schema.StuCourse, None]):
    def create(self, db: Session, *, obj_in: class_schema.Student) -> Student:
        db_obj = Student(
            name=obj_in.name,
            id = obj_in.cid,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

curd_student = CRUDStudent(Student)
