from flask import jsonify,Blueprint,Response,request
from flask_restful import reqparse,Api,Resource

from db import db
user_blue = Blueprint('user_blue',__name__)
api=Api(user_blue)

class PView(Resource):
    def post(self):
        req=reqparse.RequestParser()
        req.add_argument('payid')
        req.add_argument('name')
        req.add_argument('number')
        req.add_argument('date')
        req.add_argument('amount')
        req.add_argument('payer')
        req.add_argument('cid')
        args=req.parse_args()
        sql = "insert into payment (payid,name,number,date,amount,payer,cid) values ('%s','%s','%s','%s','%s','%s','%s')" % (args['payid'], args['name'],args['number'],args['date'],args['amount'],args['payer'],args['cid'])
        db.update(sql)
        db.commit()
        return jsonify({
                        'code':200,
                        'msg':'添加成功'
                    })

    def get(self):
        req=reqparse.RequestParser()
        req.add_argument('payid')

        args=req.parse_args()
        sql = "select * from payment where payid=%s"%args['payid']
        res = db.find_all(sql)
        db.commit()

        return jsonify({
                        'code':200,
                        'msg':'查询成功'

                    })





api.add_resource(PView,'/pay')
